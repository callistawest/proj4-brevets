# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Background

This program focuses on brevets, calculating accurate brevet times for a race dependent on calculations from https://rusa.org/pages/acp-brevet-control-times-calculator.

For each category of distance, there is a top speed and bottom speed. As the distance of the brevet increases, both the top and bottom speed decrease.

Top speed expected:
0-200km= 34 km/hr
200-400km = 32 km/hr
400-600km = 30 km/hr
600-1000km = 28km/hr

Bottom Speed expected:
0-200km = 15km/hr
200-400km = 15km/hr
400-600 = 11.4 km/hr
600-1000 = 13.3 km/hr

## Running flask_brevets.py

When running the command, "python3 flask_brevets.py", the server will run on https://0.0.0.0:5000. On the ACP Controle Times webpage, the distance, time and date can be control at the top of the page. Additionally, adding milage to the miles category will produce the correct km value, the open time for the brevet as well as the close time of the brevet.

These categories of the table will be filled in automatically after pressing enter for the miles category. 

## Testing

Testing will be completed using Nosetests. Nosetests will call test_acp.py. Within test_acp.py, there are various test cases, testing brevet intervals in km. The test cases include km distances that end on the edge of two interval categories (such as 200 and 400), as well as testing different dates and distances including decimals.

## Credentials

Callista West

CIS 322 Fall 2020

cwest10@uoregon.edu
