"""
Callista West, CIS 322
Working Version
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###

#untouched from starter code
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

#updated _calc_times using rusa.org
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', -1, type=float)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    distance = request.args.get('distance') 
    app.logger.debug("request.args: {}".format(request.args))

    if km == -1:
        result = {"error": 2}
        return flask.jsonify(result=result)

    distance = float(distance)
    # Convert distance to float
    if km > distance * 1.2:     
        result = {"error": 1}
        return flask.jsonify(result=result)                             # Abort now with error

    time = begin_date + " " + begin_time
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)
    
    #using jsonify to return
    result = {"open": open_time, "close": close_time, "error": 0}
    return flask.jsonify(result=result)

#############
# remainder of program left untouched from skeleton code
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
