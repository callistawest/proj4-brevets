"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

#using similar logic for open_time and close_time

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if control_dist_km > brevet_dist_km:                                        
        control_dist_km = brevet_dist_km                                        
    control_open_time = 0                                              
    # Open time unit = hours 
    alt_distance = control_dist_km 
    open_dict = {200:34, 400:32, 600:30, 1000:28, 1300:26}
    last_distance = 0
    while control_dist_km > 0: 
       for distance in open_dict:  
          if control_dist_km > distance: 
             control_open_time += (distance-last_distance)/open_dict[distance]   
             # Store and add the size of that distance boundary divided by its speed to the finish time
             alt_distance -= (distance-last_distance)                            
             # keep the remainder saved
             last_distance = distance                                          
          else:                                                                   
          # Distance is between the current boundary and last boundary
                control_open_time += alt_distance/open_dict[distance]               
                control_dist_km = 0                                                 
                break                                                               
                # there is no distance remaining
    hours = int(control_open_time)                                                  
    minutes = int((control_open_time*60)%60)                                       
    seconds = int((control_open_time*3600)%60)                                      
    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')                    
    date_time = date_time.shift(hours=+hours, minutes=+minutes)                     
    date_time = date_time.replace(tzinfo='US/Pacific')                              
    # Converting and changing timezone to PST
    if seconds >= 30:
        date_time = date_time.shift(minutes=+1)                                     
        # barely but changing start time by rounding up the start time to the nearest minute
    return date_time.isoformat()

#using similar logic compared to open_time
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if control_dist_km > brevet_dist_km:                                            
    # If the control distance is more than total brevet distance
        control_dist_km = brevet_dist_km                                            
        # using total brevet distance
    special_dict = {1000:4500, 600:2400, 400:1620, 300:1200, 200:810}
    control_close_time = 0                                                          
    # Close time units = hours.
    alt_distance = control_dist_km
    temp = control_dist_km                                                          
    close_dict = {600:15, 1000:11.428, 1300:13.333}                                 
    last_distance = 0                                                               
    while control_dist_km > 0:                                                      
        for distance in close_dict:
            if control_dist_km > distance:                                          
            # If the distance is larger than current distance boundary
                control_close_time += (distance-last_distance)/close_dict[distance]
                alt_distance -= (distance-last_distance)
                last_distance = distance                                            
            else:                                                                   
                control_close_time += alt_distance/close_dict[distance]             
                control_dist_km = 0                                                 
                break                                                               
    hours = int(control_close_time)                                                 
    # Get stored integer of control_close_time as the hour value
    minutes = int((control_close_time*60)%60)
    seconds = int((control_close_time*3600)%60)                                     
    if temp == 0:                                                                   
    # If checkpoint distance is 0
        hours = 1                                                                   
    if temp == brevet_dist_km:                                                      
        hours = seconds = 0 
        minutes = special_dict[brevet_dist_km]
    date_time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')                    
    date_time = date_time.shift(hours=+hours, minutes=+minutes)                     
    # Shift start time
    date_time = date_time.replace(tzinfo='US/Pacific')                              
    # Set timezone to PST
    if seconds >= 30:
        date_time = date_time.shift(minutes=+1)                                     
        # Round up the start time to the nearest minute
    return date_time.isoformat()


